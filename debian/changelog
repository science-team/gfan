gfan (0.7-2) unstable; urgency=medium

  * debian/patches/int128.patch
    - New patch; use Abseil to provide 128-bit integers on 32-bit
      architectures that don't support them natively.
  * debian/control
    - Add libabsl-dev to Build-Depends; needed for new 128-bit integer
      patch.

 -- Doug Torrance <dtorrance@debian.org>  Thu, 24 Oct 2024 11:25:53 -0400

gfan (0.7-1) unstable; urgency=medium

  * New upstream release.
  * debian/control
    - Bump Standards-Version to 4.7.0.
  * debian/patches
    - Refresh for new upstream release.
    - Remove set_locale_test_0008.patch, 64_bit_int.patch, and
      find-name.patch; applied upstream.
  * debian/patches/cstdint.patch
    - New patch; include <cstdint> for std::int64_t.
  * debian/patches/dont_forget_flags_debian.patch
    - Move a couple essential flags from now-unused OPTFLAGS to CPPFLAGS.
  * debian/watch
    - Add uversionmangle option to prepend ~ before beta, etc.

 -- Doug Torrance <dtorrance@debian.org>  Thu, 22 Aug 2024 06:54:03 -0400

gfan (0.6.2-7) unstable; urgency=medium

  * debian/control
    - Bump Standards-Version to 4.6.2.
    - Update my email address (now a Debian developer).
  * debian/copyright
    - Update my copyright years and email address.
  * debian/patches/*
    - Update my email address.
  * debian/patches/remove_failing_tests_on_32bits.patch
    - Restore patch.
  * debian/rules
    - Stop adding -ffloat-store flag; not working with GCC 13.
  * debian/source/lintian-overrides
    - New file; override false positive source-is-missing error.

 -- Doug Torrance <dtorrance@debian.org>  Sat, 23 Mar 2024 23:35:04 -0400

gfan (0.6.2-6) unstable; urgency=medium

  * debian/patches/find-name.patch
    - Add Forwarded field.
  * debian/patches/remove_failing_tests_on_32bits.patch
    - Remove patch; no longer needed.
  * debian/rules
    - Add -ffloat-store to CFLAGS on 32-bit architectures; fixes
      0009RenderStairCase test.
    - Convert override_* to execute_before_* where possible.
    - Build documentation a second time to fix references.

 -- Doug Torrance <dtorrance@piedmont.edu>  Sun, 21 Nov 2021 23:51:26 -0500

gfan (0.6.2-5) unstable; urgency=medium

  [ Andreas Tille ]
  * Fix spelling
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ Doug Torrance ]
  * debian/control
    - Update Maintainer to Debian Math Team.
    - Update Vcs-* fields (science-team -> math-team).
    - Bump Standards-Version to 4.6.0.
  * debian/copyright
    - Add myself to copyright holders for debian/*.
  * debian/patches/find-name.patch
    - Properly terminate while loop when finding application name.
  * debian/rules
    - Stop including architecture.mk; not used.
    - Move code for generating manpage to one place.
    - Stop setting DH_VERBOSE.

 -- Doug Torrance <dtorrance@piedmont.edu>  Mon, 08 Nov 2021 19:33:55 -0500

gfan (0.6.2-4) unstable; urgency=medium

  * debian/control
    - Drop versioned dependency on libcdd-dev; the minimum version was
      094g, but that is in oldoldstable.
  * debian/patches/remove_failing_tests_on_32.bits.patch
    - Restore patch; we will return to just skipping this test, as we
      can't predict the expected output based on 32-bit v. 64-bit
      architecture alone.
  * debian/patches/render-staircase.patch
    - Remove patch.
  * debian/rules
    - Stop patching the 0009RenderStairCase test on 32-bit architectures.
    - Refactor override_dh_link target.  Previously, we called dh_link
      once for each app to create a symlink for the manpage.  However,
      each individual call was recreating the symlinks to the apps
      themselves, resulting in nearly 9000 calls to rm and ln!  We
      reduce this down to a single call to dh_link, removing all of the
      redundancies.

 -- Doug Torrance <dtorrance@piedmont.edu>  Thu, 03 Dec 2020 20:20:45 -0500

gfan (0.6.2-3) unstable; urgency=medium

  * debian/clean
    - Remove generated manpage during dh_clean.
  * debian/compat
    - Remove file; we now set the compatibility level with
      debhelper-compat.
  * debian/control
    - Bump Standards-Version to 4.5.1.
    - Add Rules-Requires-Root field.
    - Update Homepage.
    - Bump to debhelper compatibility level 13.
  * debian/copyright
    - Update Source.
  * debian/gfan.1.in
    - Rename file; the manpage is now generated during build.
    - Use special characters for umlauts to avoid troff warnings.
  * debian/gfan.install
    - New file; install testsuite.
  * debian/patches
    - Add Forwarded tags where needed.
  * debian/patches/64_bit_int.patch
    - Use int64_t instead of long for 64-bit integers; the overflow
      on 32-bit systems was causing an infinite loop in a test
      (Closes: #974558).
  * debian/patches/dont_forget_flags_debian.patch
    - Don't forget CPPFLAGS for symmetrictraversal.cpp.
  * debian/patches/{fix_spelling_errors,make_tests_return_error}.patch
    - Don't reintroduce a typo in a later patch.
  * debian/patches/no_manual_date.patch
    - Remove patch; no longer necessary since LaTeX supports
      SOURCE_DATE_EPOCH.
  * debian/patches/render-staircase.patch
    - New patch; fix failing test on 32-bit architectures.
  * debian/rules
    - Remove 0602ResultantFanProjection from tests to skip on 32-bit
      architectures; the underlying problem has been fixed and it no
      longer fails.
    - New override_dh_installman target; generate manpage during
      build using the source date and version number.
    - Export FORCE_SOURCE_DATE so LaTeX uses SOURCE_DATE_EPOCH and we
      get a reproducible build of the manual.
    - Add override_dh_install target; don't install temporary files
      from testsuite directory.
    - Instead of skipping a failing test on 32-bit architectures, we
      now apply a patch to fix it.
  * debian/salsa-ci.yml
    - Add Salsa pipelines config file.
  * debian/tests/control
    - Use installed testsuite directory.
  * debian/watch
    - Update url.

 -- Doug Torrance <dtorrance@piedmont.edu>  Sat, 21 Nov 2020 22:00:45 -0500

gfan (0.6.2-2) unstable; urgency=medium

  * debian/patches/no_manual_date.patch
    - Use empty date in manual for reproducible builds.
  * debian/tests/control
    - Specify executable to use for tests.  Otherwise the default
      (./gfan) is used, which only works during build and not during
      DEP-8 tests.

 -- Doug Torrance <dtorrance@piedmont.edu>  Mon, 24 Sep 2018 07:44:07 -0400

gfan (0.6.2-1) unstable; urgency=medium

  * Upload to unstable.
  * debian/control
    - Bump Standards-Version to 4.2.1.

 -- Doug Torrance <dtorrance@piedmont.edu>  Wed, 05 Sep 2018 16:11:52 -0400

gfan (0.6.2-1~exp2) experimental; urgency=medium

  * debian/control
    - Bump Standards-Version to 4.2.0.
  * debian/patches/remove_failing_tests_on_32bits.patch
    - Remove patch.
  * debian/rules
    - Remove tests which fail on 32-bit architectures.  Previously, we removed
      0009RenderStairCase on *all* architectures using quilt.  We now use
      dpkg-architecture to only remove this and the newly-introduced
      0602ResultantFanProjection on the architectures on which they fail
      (Closes: #905300).

 -- Doug Torrance <dtorrance@piedmont.edu>  Wed, 22 Aug 2018 14:37:22 -0400

gfan (0.6.2-1~exp1) experimental; urgency=medium

  [ Andreas Tille ]
  * Homepage is not part of UpstreamMetadata any more since it is redundant

  [ Doug Torrance ]
  * New upstream release.
    - Manual is now no longer released under a non-DFSG license.
  * debian/clean
    - Update location of GfAnTeMpTeStS file to clean.
    - Add files generated during build of manual.
  * debian/compat
    - Bump to debhelper compatibility level 11.
  * debian/control
    - Tidy up using wrap-and-sort.
    - Switch priority to optional.
    - Bump debhelper in Build-Depends to >= 11.
    - Remove dpkg-dev (>= 1.16.1~) from Build-Depends. This was required for
      dpkg-buildflags before debhelper 9.
    - Replace libgmp3-dev (a dummy package) with libgmp-dev in Build-Depends.
    - Add html2text to Build-Depends; needed to generate upstream changelog.
    - Add ghostscript, texlive-latex-base, and texlive-latex-recommended to
      Build-Depends for building manual.
    - Bump Standards-Version to 4.1.5.
    - Update Homepage.
    - Update Vcs-* fields to new Salsa URLs.
  * debian/control.in
    - Remove unused file.
  * debian/copyright
    - Use https in Format.
    - Update Source url.
    - Remove Comment, as we no longer remove the doc directory.
    - Change License to GPL-2+ to properly reflect the copyright license
      and update license text.
  * debian/gbp.conf
    - Remove file as we are no longer repacking orig tarball.
  * debian/gfan.doc-base
    - New file; register manual.
  * debian/gfan.docs
    - New file; install manual.
  * debian/gfan.lintian-overrides
    - Remove file; no longer necessary as we now install an upstream changelog.
  * debian/patches
    - Remove patches which have been applied or the issues otherwise fixed
      in the new upstream release.
    - Refresh remaining patches for new upstream release.
  * debian/patches/make_tests_return_error.patch
    - New patch from Sage; detect failing tests.
  * debian/patches/remove_failing_tests_on_32bits.patch
    - Completely remove failing test. An empty test caused 'no such file'
      errors.
  * debian/patches/set_locale_test_0008.patch
    - Update patch with feature from Sage; remove lines to improve sorting.
  * debian/README.source
    - Remove file; described the process of repacking the tarball without the
      doc directory.
  * debian/rules
    - Remove pre-debhelper 9 method of invoking dpkg-buildflags.
    - Remove override_dh_auto_test target. It was no longer unnecessary,
      as upstream added a check target to the Makefile.  This has the
      additional benefit of adding support for DEB_BUILD_OPTIONS=nocheck
      (Closes: #880928).
    - New override_dh_installchangelogs target. Generates upstream changelog
      from homepage source.
    - New override_dh_installdocs target; build manual.
  * debian/tests/control
    - Add test suite.
  * debian/watch
    - Bump to uscan v4 and use special strings.
    - Remove dversionmangle option; we no longer need to repackage the orig
      tarball.

 -- Doug Torrance <dtorrance@piedmont.edu>  Wed, 27 Jun 2018 15:50:03 -0400

gfan (0.5+dfsg-6) unstable; urgency=medium

  * debian/control:
    + Add myself to Uploaders.
    + Add Vcs-* fields.
    + Bump Standards-Version to 3.9.8.
  * debian/patches/fix_spelling_errors.patch:
    + Fix additional typos.
  * debian/patches/gcc6_compat.patch:
    + New patch borrowed from Gentoo. Fixes ambiguous reference errors when
      compiled with gcc-6 (Closes: #811805).
  * debian/rules
    + Add all hardening flags.

 -- Doug Torrance <dtorrance@piedmont.edu>  Wed, 13 Jul 2016 20:22:04 -0400

gfan (0.5+dfsg-5) unstable; urgency=medium

  * debian/patches:
    + add fix_compilation_clang.diff to properly forward declare some classes
      (Closes: #755308)
    + add fix_other_warnings_clang.diff to remove some other warnings about
      format strings and a typo in a macro name
  * debian/copyright: fix upstream email address
  * Bump Standards-Version to 3.9.6 (no changes needed)
  * Move upstream info to debian/upstream/metadata

 -- Cédric Boutillier <boutil@debian.org>  Sun, 26 Oct 2014 18:54:17 +0100

gfan (0.5+dfsg-4) unstable; urgency=low

  * add fix_GetUniqueGenerators_symmetry.patch to fix a range error causing a
    segmentation fault on powerpc.

 -- Cédric Boutillier <boutil@debian.org>  Mon, 23 Sep 2013 15:04:31 +0200

gfan (0.5+dfsg-3) unstable; urgency=low

  * Upload to unstable.
  * modify remove_failing_tests_on_32bits.patch to replace command of
    0009RenderStairCase test with an empty one instead of deleting it.
  * remove lintian override about spelling error

 -- Cédric Boutillier <boutil@debian.org>  Tue, 09 Jul 2013 10:44:01 +0200

gfan (0.5+dfsg-2) experimental; urgency=low

  * Set priority to extra instead of optional
  * Incorporate and acknowledge changes from NMU 0.3dfsg-1.1. Thanks Gregor
    Herrmann.
  * debian/patches:
    + add remove_failing_tests_on_32bits.patch to circumvent a FTBFS described
      in bug #703035
  * add debian/upstream

 -- Cédric Boutillier <boutil@debian.org>  Thu, 14 Mar 2013 23:47:30 +0100

gfan (0.5+dfsg-1) experimental; urgency=low

  * New upstream version.
    + repack upstream tarball to remove non-free doc/ directory
    + setup debian/gbp.conf to automatically filter out doc/ for next versions
  * debian/patches:
    + remove gfan-installlinks-relative.patch: applied upstream
    + add remove_extra_declaration.patch: left-over declaration causing error
    + add use_system_cdd.patch: cdd headers are in /usr/include/cdd
    + set_locale_test_0008.patch: fix locale for consistency in sorted output
    + fix_command_test_0056.patch: use relative path for func.poly file
    + string_subst_test_50x.patch: replace gfan by %s as in other tests
    + dont_forget_flags_debian.patch: take into account default flags set by
      Debian
    + fix_spelling_errors.patch: fix some spelling errors in displayed
      messages of the program
  * debian/control:
    + add myself to Uploaders:, set Maintainer: to Debian Science Maintainers
    + update homepage url
    + bump Standards-Version" to 3.9.4 (no particular change needed)
  * debian/rules:
    + use dh as a packaging helper instead of cdbs
    + use hardening flags
  * debian/copyright:
    + convert to DEP-5 copyright-format/1.0
  * add debian/watch file
  * debian/source/local-options: add unapply-patches
  * override lintian message about missing upstream changelog
  * debian/gfan.1: rewrite to give a general presentation of gfan.

 -- Cédric Boutillier <boutil@debian.org>  Sat, 16 Feb 2013 16:41:16 +0100

gfan (0.3dfsg-1.1) unstable; urgency=low

   * Non-maintainer upload.
   * Fix "FTBFS: application.cpp:548:22: error: format not a string
     literal and no format arguments [-Werror=format-security]":
     add patch fix-string-format-error.patch from Eric Alexander.
     (Closes: #643383)

 -- gregor herrmann <gregoa@debian.org>  Sun, 20 Nov 2011 15:10:13 +0100

gfan (0.3dfsg-1) unstable; urgency=low

  * Initial release (Closes: #480089).

 -- Tim Abbott <tabbott@mit.edu>  Thu, 05 Jun 2008 17:47:51 -0400
